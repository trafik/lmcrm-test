-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Фев 02 2018 г., 12:48
-- Версия сервера: 5.7.17-0ubuntu0.16.04.1
-- Версия PHP: 7.1.2-4+deb.sury.org~xenial+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `lmcrm`
--

-- --------------------------------------------------------

--
-- Структура таблицы `customers`
--

CREATE TABLE `customers` (
  `id` int(10) UNSIGNED NOT NULL,
  `phone` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `customers`
--

INSERT INTO `customers` (`id`, `phone`, `created_at`, `updated_at`) VALUES
(1, 50455656, '2018-02-01 16:24:57', '2018-02-01 16:24:59'),
(2, 507467893, '2018-02-01 16:25:01', '2018-02-01 16:25:03');

-- --------------------------------------------------------

--
-- Структура таблицы `leads`
--

CREATE TABLE `leads` (
  `id` int(10) UNSIGNED NOT NULL,
  `agent_id` int(11) NOT NULL,
  `sphere_id` int(11) NOT NULL,
  `opened` int(11) NOT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `customer_id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci,
  `bad` int(11) DEFAULT NULL,
  `date` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `leads`
--

INSERT INTO `leads` (`id`, `agent_id`, `sphere_id`, `opened`, `email`, `customer_id`, `name`, `comment`, `bad`, `date`, `created_at`, `updated_at`) VALUES
(7, 14, 4, 2, '1_leed@mail.com', 1, 'lead', '', 0, '2018-02-01', '2018-02-01 16:22:30', '2018-02-01 16:22:32'),
(8, 10, 4, 1, '2_leed@mail.com', 1, '2_lead', '', 0, '2018-02-02', '2018-02-01 16:22:30', '2018-02-01 16:22:32'),
(9, 9, 4, 0, '3_leed@mail.com', 1, 'lead 3', '', 0, '2018-02-03', '2018-02-01 16:22:30', '2018-02-01 16:22:32');

-- --------------------------------------------------------

--
-- Структура таблицы `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_02_01_110625_create_table_customers', 1),
(4, '2018_02_01_110734_create_table_leads', 1),
(5, '2018_02_01_111425_create_table_open_leads', 1),
(6, '2018_02_01_111602_create_table_open_sphere_attributes', 1),
(7, '2018_02_01_112058_create_table_open_sphere_attribute_options', 1),
(8, '2018_02_01_113550_create_table_open_sphere_bitmask__x_x', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `open_leads`
--

CREATE TABLE `open_leads` (
  `id` int(10) UNSIGNED NOT NULL,
  `lead_id` int(11) NOT NULL,
  `agent_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `open_leads`
--

INSERT INTO `open_leads` (`id`, `lead_id`, `agent_id`, `created_at`, `updated_at`) VALUES
(1, 7, 14, '2018-02-01 16:31:34', '2018-02-01 16:31:35'),
(2, 8, 14, '2018-02-01 16:31:37', '2018-02-01 16:31:37');

-- --------------------------------------------------------

--
-- Структура таблицы `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `sphere_attributes`
--

CREATE TABLE `sphere_attributes` (
  `id` int(10) UNSIGNED NOT NULL,
  `sphere_id` int(11) NOT NULL,
  `_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `label` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `required` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `default_value` int(11) NOT NULL,
  `position` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `sphere_attributes`
--

INSERT INTO `sphere_attributes` (`id`, `sphere_id`, `_type`, `label`, `icon`, `required`, `default_value`, `position`, `created_at`, `updated_at`) VALUES
(34, 4, 'radio', 'Radio', NULL, NULL, 1111, 1, '2018-02-01 21:17:08', '2018-02-01 21:17:09'),
(35, 4, 'checkbox', 'CheckBox', NULL, NULL, 222, 1, '2018-02-01 21:17:08', '2018-02-01 21:17:09');

-- --------------------------------------------------------

--
-- Структура таблицы `sphere_attribute_options`
--

CREATE TABLE `sphere_attribute_options` (
  `id` int(10) UNSIGNED NOT NULL,
  `sphere_attr_id` int(11) NOT NULL,
  `ctype` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `sphere_attribute_options`
--

INSERT INTO `sphere_attribute_options` (`id`, `sphere_attr_id`, `ctype`, `_type`, `name`, `value`, `position`, `created_at`, `updated_at`) VALUES
(3, 34, 'agent', 'option', 'r1-n', 'r1', NULL, '2018-02-01 21:36:53', '2018-02-01 21:36:55'),
(4, 34, 'agent', 'option', 'r2-n', 'r2', NULL, '2018-02-01 21:36:53', '2018-02-01 21:36:55'),
(5, 34, 'agent', 'option', 'r3-n', 'r3', NULL, '2018-02-01 21:36:53', '2018-02-01 21:36:55'),
(6, 35, 'agent', 'option', 'c1-n', 'c1', NULL, '2018-02-01 21:36:53', '2018-02-01 21:36:55'),
(7, 35, 'agent', 'option', 'c1-n', 'c2', NULL, '2018-02-01 21:36:53', '2018-02-01 21:36:55'),
(8, 35, 'agent', 'option', 'c1-n', 'c3', NULL, '2018-02-01 21:36:53', '2018-02-01 21:36:55');

-- --------------------------------------------------------

--
-- Структура таблицы `sphere_bitmask_4`
--

CREATE TABLE `sphere_bitmask_4` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `type` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL,
  `lead_price` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `fb_34_3` int(11) DEFAULT NULL,
  `fb_34_4` int(11) DEFAULT NULL,
  `fb_34_5` int(11) DEFAULT NULL,
  `fb_35_6` int(11) DEFAULT NULL,
  `fb_35_7` int(11) DEFAULT NULL,
  `fb_35_8` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `sphere_bitmask_4`
--

INSERT INTO `sphere_bitmask_4` (`id`, `user_id`, `type`, `status`, `lead_price`, `created_at`, `updated_at`, `fb_34_3`, `fb_34_4`, `fb_34_5`, `fb_35_6`, `fb_35_7`, `fb_35_8`) VALUES
(1, 1, 'agent', 1, 37, '2018-02-01 22:45:17', '2018-02-01 22:45:19', 1, 1, 1, NULL, NULL, NULL),
(2, 14, 'agent', 1, 35, '2018-02-01 22:45:17', '2018-02-01 22:45:19', 1, 1, 0, NULL, NULL, NULL),
(3, 7, 'lead', 0, NULL, '2018-02-01 22:45:17', '2018-02-01 22:45:19', 1, 0, 0, 1, 1, 1),
(4, 8, 'lead', 0, NULL, '2018-02-01 22:45:17', '2018-02-01 22:45:19', 0, 1, 1, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `leads`
--
ALTER TABLE `leads`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `open_leads`
--
ALTER TABLE `open_leads`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Индексы таблицы `sphere_attributes`
--
ALTER TABLE `sphere_attributes`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `sphere_attribute_options`
--
ALTER TABLE `sphere_attribute_options`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `sphere_bitmask_4`
--
ALTER TABLE `sphere_bitmask_4`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `leads`
--
ALTER TABLE `leads`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT для таблицы `open_leads`
--
ALTER TABLE `open_leads`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `sphere_attributes`
--
ALTER TABLE `sphere_attributes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT для таблицы `sphere_attribute_options`
--
ALTER TABLE `sphere_attribute_options`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT для таблицы `sphere_bitmask_4`
--
ALTER TABLE `sphere_bitmask_4`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
