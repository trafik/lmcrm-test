<?php

namespace App\Http\Controllers;

use App\Lead;
use App\OpenLead;
use App\SphereBitmask;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LeadController extends Controller
{
    public function index (OpenLead $openLead, Lead $lead)
    {



        $customer = $openLead->all();

        return view('lead.index', compact('customer'));
    }


    public function show (Request $request, OpenLead $openLead, Lead $lead)
    {



        if ($request->ajax() and csrf_token() == $request->post('_token')) {

            $item = $openLead->leadId($request->post('id'));

            $sphere_id = $item->lead->sphereAttributes->first()->sphere_id;
            $lead::setTableNameBitmask($sphere_id);

            $options = $this->getOptions($item);

            return view('lead.show', compact('item', 'options'));
        }
        abort(404);
    }


    public function getOptions ($item)
    {

        $sphereBitmask = $item->lead->sphereBitmask;
        $atribut = [];


        foreach ($item->lead->sphereAttributes as $sphereAttribute) {
            //dump($sphereAttribute->sphereOptions);
            $Options = null;
            foreach ($sphereAttribute->sphereOptions as $sphereOption) {

                if ($sphereBitmask->{'fb_'.$sphereAttribute->id.'_'.$sphereOption->id} == 1) {

                    $Options .= ' '. $sphereOption->value;
                }

            }

            $atribut[$sphereAttribute->label] = $Options;
        }

        return $atribut;

    }


}
