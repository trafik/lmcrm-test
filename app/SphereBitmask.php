<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SphereBitmask extends Model
{
    /**
     * SphereBitmask constructor.
     */
    public function __construct () {
        parent::__construct();

        $this->table = Lead::getTableNameBitmask();
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeLead ($query)
    {
        return $query->where('type', 'lead');
    }

}
