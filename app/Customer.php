<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    public function lead()
    {
        return $this->hasMany(Lead::class);
    }
}
