<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SphereAttribute extends Model
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function sphereOptions ()
    {
        return $this->hasMany(SphereAttributeOption::class, 'sphere_attr_id', 'id')->agent();
    }


}
