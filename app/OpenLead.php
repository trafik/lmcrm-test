<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OpenLead extends Model
{
    public function lead()
    {
        return $this->belongsTo(Lead::class, 'lead_id');
    }

    public function scopeLeadId ($query, $id)
    {
        return $query->where('lead_id', $id)->first();
    }
}
