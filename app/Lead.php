<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lead extends Model
{

    protected static $name_table_bitmask;
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     *
     */
//    public function openLead()
//    {
//        return $this->hasOne(OpenLead::class, 'lead_id', 'id');
//    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function customer ()
    {
        return $this->belongsTo(Customer::class);
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function sphereAttributes ()
    {
        return $this->hasMany(SphereAttribute::class, 'sphere_id', 'sphere_id');
    }


    /**
     * @return mixed
     */
    public static function getTableNameBitmask () {

        return self::$name_table_bitmask;

    }

    /**
     * @param $sphere_id
     */
    public static function setTableNameBitmask ($sphere_id) {

        self::$name_table_bitmask = 'sphere_bitmask_'. $sphere_id;
    }


    /**
     * @return mixed
     */
    public function sphereBitmask ()
    {
        return $this->hasOne(SphereBitmask::class, 'user_id', 'id')->lead();
    }


}
