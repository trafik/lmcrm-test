<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SphereAttributeOption extends Model
{
    /**
     * @param $query
     * @return mixed
     */
    public function scopeAgent($query)
    {
        return $query->where('ctype', 'agent');
    }

}
