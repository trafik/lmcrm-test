<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableOpenSphereAttributeOptions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sphere_attribute_options', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sphere_attr_id');
            $table->string('ctype', 20);
            $table->string('_type', 20);
            $table->string('name', 20);
            $table->string('value', 20);
            $table->string('position', 20);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sphere_attribute_options');
    }
}
