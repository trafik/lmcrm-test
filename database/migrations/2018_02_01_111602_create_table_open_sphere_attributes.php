<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableOpenSphereAttributes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sphere_attributes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sphere_id');
            $table->string('_type', 20);
            $table->string('label', 20);
            $table->string('icon')->nullable();
            $table->string('required')->nullable();
            $table->integer('default_value');
            $table->integer('position');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sphere_attributes');
    }
}
