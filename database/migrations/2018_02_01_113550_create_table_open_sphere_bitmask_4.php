<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableOpenSphereBitmask4 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sphere_bitmask_4', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('type');
            $table->integer('status');
            $table->integer('lead_price')->nullable();
            $table->timestamps();
            $table->integer('fb_34_3')->nullable();
            $table->integer('fb_34_4')->nullable();
            $table->integer('fb_34_5')->nullable();
            $table->integer('fb_35_6')->nullable();
            $table->integer('fb_35_7')->nullable();
            $table->integer('fb_35_8')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sphere_bitmask_4');
    }
}
