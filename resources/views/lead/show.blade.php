<table class="table table-bordered table-show">
    <tr>
        <td>icon:</td>
        <td></td>
    </tr>
    <tr>
        <td>date:</td>
        <td>{{$item->lead->date}}</td>
    </tr>
    <tr>
        <td>name:</td>
        <td>{{$item->lead->name}}</td>
    </tr>
    <tr>
        <td>phone:</td>
        <td>{{$item->lead->customer->phone}}</td>
    </tr>
    <tr>
        <td>email:</td>
        <td>{{$item->lead->email}}</td>
    </tr>

    @foreach($options as $label => $row)
        <tr>
            <td>{{$label}}</td>
            <td>
                {{$row}}

            </td>
        </tr>
    @endforeach
</table>