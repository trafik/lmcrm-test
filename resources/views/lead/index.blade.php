<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{!! csrf_token() !!}" />
    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
    <!-- Styles -->
    <style>
        .table-show tr td:first-child {
            font-weight: bold;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-6">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th>icon</th>
                    <th>date</th>
                    <th>name</th>
                    <th>phone</th>
                    <th>email</th>
                </tr>
                </thead>
                <tbody>

                @foreach($customer as $item)
                    {{--{{dd($item->lead)}}--}}
                    <tr data-id="{{$item->lead->id}}">
                        <td></td>
                        <td>{{$item->lead->date}}</td>
                        <td>{{$item->lead->name}}</td>
                        <td>{{$item->lead->customer->phone}}</td>
                        <td>{{$item->lead->email}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="show"></div>
        </div>
    </div>
</div>

</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>

<script type="text/javascript">

    $(document).ready(function () {

        $(document).on('click', 'table tbody tr', function () {

            var id = $(this).data('id');
            $.ajax({
                type: "POST",
                url: 'show',
                data: {"id": id, "_token" : '{{csrf_token()}}'},
                success: function (msg) {
                    $('.show').html(msg);
                }
            })

        });

    });

</script>

</html>
